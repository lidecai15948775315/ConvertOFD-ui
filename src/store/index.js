import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    token: ""
  },
  setToken(state, token) {
    state.token = token;
  },
  getters: {
    getToken: state => state.token
  },
  mutations: {},
  actions: {},
  modules: {}
});
