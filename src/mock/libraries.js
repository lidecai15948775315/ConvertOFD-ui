export default {
    "libraries": [
      {
        "description": "文书档案",
        "icon": "md-document",
        "id": 881,
        "levels": 1,
        "name": "文书档案",
        "table_aj": null,
        "table_jn": 486
      },
      {
        "description": "许可档案",
        "icon": "md-document",
        "id": 342,
        "levels": 2,
        "name": "许可档案",
        "table_aj": 148,
        "table_jn": 149
      },
      {
        "description": "执法档案",
        "icon": "md-document",
        "id": 247,
        "levels": 2,
        "name": "执法档案",
        "table_aj": 113,
        "table_jn": 114
      },
      {
        "description": "邮票设计档案",
        "icon": "md-contacts",
        "id": 41,
        "levels": 1,
        "name": "邮票设计档案",
        "table_aj": null,
        "table_jn": 28
      },
      {
        "description": "实物档案",
        "icon": "md-globe",
        "id": 4,
        "levels": 2,
        "name": "实物档案",
        "table_aj": 13,
        "table_jn": 14
      },
      {
        "description": "科技档案",
        "icon": "md-globe",
        "id": 3,
        "levels": 2,
        "name": "科技档案",
        "table_aj": 11,
        "table_jn": 12
      }
    ],
    "state": "ok"
  }