import axios from "axios";
import qs from "qs";
class HttpRequest {
  constructor(baseUrl = baseUrl) {
    this.baseUrl = baseUrl;
    this.queue = {};
  }
  getInsideConfig() {
    const config = {
      baseURL: this.baseUrl,
      headers: {
        //
      }
    };
    return config;
  }
  distroy(url) {
    delete this.queue[url];
    if (!Object.keys(this.queue).length) {
      // Spin.hide()
    }
  }
  interceptors(instance, url) {
    // 请求拦截
    instance.interceptors.request.use(
      config => {
        // 添加全局的loading...
        if (!Object.keys(this.queue).length) {
          // Spin.show() // 不建议开启，因为界面不友好
        }
        this.queue[url] = true;
        return config;
      },
      error => {
        return Promise.reject(error);
      }
    );
    // 响应拦截
    instance.interceptors.response.use(
      res => {
        this.distroy(url);
        const { data, status } = res;
        return { data, status };
      },
      error => {
        this.distroy(url);
        return Promise.reject(error);
      }
    );
  }
  request(options) {
    const instance = axios.create({
      timeout: 30000,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
      },
      transformRequest: [
        function(data) {
          return qs.stringify(data, { allowDots: true, indices: true });
        }
      ],
      paramsSerializer: function(data) {
        return qs.stringify(data, { allowDots: true, indices: true });
      }
    });
    options = Object.assign(this.getInsideConfig(), options);
    this.interceptors(instance, options.url);
    return instance(options);
  }
}
export default HttpRequest;
