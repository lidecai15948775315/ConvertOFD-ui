import axios from '@/libs/api.request'
const SERVICE_URL = '/ConvertOFD'

export const getOfdJson = () => {
    return axios.request({
        url: SERVICE_URL + '/getOfdData',
        method: 'post'
    })
}