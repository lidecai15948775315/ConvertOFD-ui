module.exports = {
    //使pdfjs编译通过
    configureWebpack: {
        resolve: {
            symlinks: false
        }
    },
    productionSourceMap: false,
    devServer: {
        proxy: {
            '/ConvertOFD': {
                target: 'http://localhost:8081/'
            }
        }
    }
}